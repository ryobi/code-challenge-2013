﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using ToyRobot.Model;

namespace ToyRobot.Helpers
{
    public class CommandParser : ICommandParser
    {
        public const string LeftCom = "LEFT";
        public const string RightCom = "RIGHT";
        public const string MoveCom = "MOVE";
        public const string PlaceCom = "PLACE";
        public const string ReportCom = "REPORT";
        public const string CleanCom = "CLEAN";

        public string ParseCommand(IRobot robot, string command)
        {
            var commandSegments = command.Split(' ');
            var verb = commandSegments.First();

            switch (verb.ToUpperInvariant())
            {
                case LeftCom:
                    LeftCommand(robot);
                    break;
                case RightCom:
                    RightCommand(robot);
                    break;
                case MoveCom:
                    MoveCommand(robot, string.Join(",", commandSegments.Except(new List<string> {verb})));
                    break;
                case PlaceCom:
                    PlaceCommand(robot, string.Join(",", commandSegments.Except(new List<string> {verb})));
                    break;
                case ReportCom:
                    return ReportCommand(robot);
                case CleanCom:
                    return CleanCommand(robot);
                default:
                    throw new ArgumentException("Invalid Command!");
            }

            return string.Empty;
        }

        private void LeftCommand(IRobot robot)
        {
            robot.Left();
        }

        private void RightCommand(IRobot robot)
        {
            robot.Right();
        }


        private void MoveCommand(IRobot robot, string input)
        {
            var inputPieces = input.Split(',');

            if (inputPieces.Count() > 1)
                throw new ArgumentException("Invalid arguments for " + MoveCom + "command! ");

            var stepsToMovePreParse = inputPieces.SingleOrDefault();
            int stepsToMove;

            int.TryParse(stepsToMovePreParse, out stepsToMove);

            robot.Move(stepsToMove);
        }

        private void PlaceCommand(IRobot robot, string input)
        {
            var inputPieces = input.Split(',');
            
            if (!inputPieces.Any() || inputPieces.Count() > 3)
                throw new ArgumentException("Invalid arguments for " + PlaceCom + "command! ");

            var xCoOrdsPreParse = inputPieces[0];
            int xCoOrd;
            if (!int.TryParse(xCoOrdsPreParse, out xCoOrd))
                return;

            var yCoOrdsPreParse = inputPieces[1];
            int yCoOrd;
            if (!int.TryParse(yCoOrdsPreParse, out yCoOrd))
                return;

            Direction direction;
            try
            {
                direction = ParseDirection(inputPieces[2]);
            }
            catch (ArgumentException)
            {
                return;
            }

            robot.Place(xCoOrd, yCoOrd, direction);
        }

        private string ReportCommand(IRobot robot)
        {
            return robot.Report();
        }

        private string CleanCommand(IRobot robot)
        {
            if (robot.Clean())
                return "GAME OVER";

            return String.Empty;
        }

        private Direction ParseDirection(string direction)
        {
            switch (direction.ToUpperInvariant())
            {
                case "NORTH":
                    return Direction.North;
                case "SOUTH":
                    return Direction.South;
                case "EAST":
                    return Direction.East;
                case "WEST":
                    return Direction.West;
                default:
                    throw new ArgumentException("Invalid Direction!");
            }
        }
    }
}
