﻿using ToyRobot.Model;

namespace ToyRobot.Helpers
{
    public interface ICommandParser
    {
        string ParseCommand(IRobot robot, string command);
    }
}
