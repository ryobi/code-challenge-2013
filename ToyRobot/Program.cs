﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using ToyRobot.Helpers;
using ToyRobot.Model;

namespace ToyRobot
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var commands = args.ElementAtOrDefault(0);
            var size = args.ElementAtOrDefault(1);
            var dirt = args.ElementAtOrDefault(2);

            if (string.IsNullOrWhiteSpace(commands))
                Console.WriteLine("Provide a robot placement file as a command line argument");
            else if (string.IsNullOrWhiteSpace(size))
                Console.WriteLine("Provide a table size file as a command line argument");
            else if (string.IsNullOrWhiteSpace(dirt))
                Console.WriteLine("Provide a dirt placement file as a command line argument");
            else
                DoProgram(commands, size, dirt);

            Console.WriteLine("\n\nPress any key to exit...");
            Console.ReadKey();
        }

        private static void DoProgram(string fileName, string sizeFileName, string dirtFileName)
        {
            IPlayingSurface table = null;
            ReadFile(sizeFileName, line => SetupPlayingSurface(out table, line));

            ReadFile(dirtFileName, line => AddDirtToSurface(table, line));

            IRobot robot = new Robot(table);
            ReadFile(fileName, line => DoCommands(robot, line));
        }

        private static void AddDirtToSurface(IPlayingSurface surface, string line)
        {
            var segments = line.Split(',');

            var x = Int32.Parse(segments.First());
            var y = Int32.Parse(segments.Last());

            var dirt = new DirtObsticle(surface);
            dirt.Place(x, y);

        }

        private static void SetupPlayingSurface(out IPlayingSurface surface, string line)
        {
            var segments = line.Split(',');

            var length = Int32.Parse(segments.First());
            var width = Int32.Parse(segments.Last());

            surface = new Table(length, width);
        }

        private static void DoCommands(IRobot robot, string line)
        {
            ICommandParser parser = new CommandParser();

            var report = parser.ParseCommand(robot, line);

            if (!string.IsNullOrWhiteSpace(report))
            {
                Console.Write(report + "\n");

                if (report.Equals("GAME OVER"))
                {
                    Console.WriteLine("\n\nPress any key to exit...");
                    Console.ReadKey();
                    Environment.Exit(0);
                }
            }

        }

        private static void ReadFile(string fileName, Action<string> actions)
        {
            var file = new StreamReader(fileName);
            string line;

            while ((line = file.ReadLine()) != null)
            {
                actions(line);
            }

            file.Close();
        }
    }
}
