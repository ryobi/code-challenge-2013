﻿namespace ToyRobot.Model
{
    public interface IPositionableObject
    {
        int XPosition { get; }
        int YPosition { get; }

        IPlayingSurface CurrentPlayingSurface();
        bool Place(int x, int y);
    }
}