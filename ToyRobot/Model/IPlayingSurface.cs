﻿namespace ToyRobot.Model
{
    public interface IPlayingSurface
    {
        int LowerBoundary { get; }

        int SurfaceWidth();
        int SurfaceLength();
        
        bool AddObjectToPlayingSurface(IPositionableObject obj);
        bool RemoveObjectFromPlayingSurface(IPositionableObject obj);

        bool IsObjectAtPosition(int x, int y);
        IPositionableObject GetObjectAtPosition(int x, int y);

        bool ObjectsRemaining<T>() where T : IPositionableObject;
    }
}
