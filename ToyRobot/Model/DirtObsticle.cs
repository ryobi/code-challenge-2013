﻿namespace ToyRobot.Model
{
    class DirtObsticle : PositionableObject, IObsticle
    {
        public DirtObsticle(IPlayingSurface surface) : base(surface)
        {
        }
    }
}
