﻿namespace ToyRobot.Model
{
    public interface IRobot : IPositionableObject
    {
        Direction FacingDirection { get; }
        bool Place(int x, int y, Direction directionFacing);
        void Move(int steps);
        void Left();
        void Right();

        bool Clean();
        string Report();
    }

    public enum Direction
    {
        North,
        South,
        East,
        West
    }
}
