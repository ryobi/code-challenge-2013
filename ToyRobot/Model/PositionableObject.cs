﻿namespace ToyRobot.Model
{
    public abstract class PositionableObject : IPositionableObject
    {

        #region Fields & Constants
    
        private int _xPosition;
        private int _yPosition;

        private readonly IPlayingSurface _playingSurface;

        #endregion

        #region Constructors

        protected PositionableObject(IPlayingSurface surface)
        {
            _playingSurface = surface;
            _playingSurface.AddObjectToPlayingSurface(this);
        }

        #endregion

        #region Properties



        /// <summary>
        /// The current X co-ordinate of the object
        /// </summary>
        public int XPosition
        {
            get { return _xPosition; }
            protected set
            {
                if (value < _playingSurface.LowerBoundary || value > _playingSurface.SurfaceLength())
                    return;

                _xPosition = value;
            }
        }

        /// <summary>
        /// The current Y co-ordinate of the object
        /// </summary>
        public int YPosition
        {
            get { return _yPosition; }
            protected set
            {
                if (value < _playingSurface.LowerBoundary || value > _playingSurface.SurfaceWidth())
                    return;

                _yPosition = value;
            }
        }

        protected bool IsPlaced { get; set; }

        #endregion

        #region Methods

        public IPlayingSurface CurrentPlayingSurface()
        {
            return _playingSurface;
        }

        public bool Place(int x, int y)
        {

            if (x < _playingSurface.LowerBoundary || x > _playingSurface.SurfaceLength())
                return false;

            if (y < _playingSurface.LowerBoundary || y > _playingSurface.SurfaceWidth())
                return false;

            XPosition = x;
            YPosition = y;

            IsPlaced = true;
            return true;
        }

        #endregion
    }
}
