﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ToyRobot.Model
{
    public class Table : IPlayingSurface
    {

        #region Fields

        private readonly int _length;
        private readonly int _width;

        private readonly IList<IPositionableObject> _objects;

        #endregion

        #region Constructors

        public Table(int length, int width)
        {
            _length = length;
            _width = width;

            _objects = new List<IPositionableObject>();
        }

        #endregion

        #region Properties

        public int LowerBoundary
        {
            get
            {
                return 0;
            }
        }

        #endregion

        #region Methods

        public int SurfaceWidth()
        {
            return _width;
        }

        public int SurfaceLength()
        {
            return _length;
        }

        public bool AddObjectToPlayingSurface(IPositionableObject obj)
        {
            if (!IsObjectAtPosition(obj.XPosition, obj.YPosition))
            {
                _objects.Add(obj);
                return true;
            }

            return false;
        }

        public bool RemoveObjectFromPlayingSurface(IPositionableObject obj)
        {
            if (IsObjectAtPosition(obj.XPosition, obj.YPosition))
            {
                _objects.Remove(obj);
                return true;
            }

            return false;
        }

        public bool IsObjectAtPosition(int x, int y)
        {
            return _objects.SingleOrDefault(obj => obj.XPosition == x && obj.YPosition == y) != null;
        }

        public IPositionableObject GetObjectAtPosition(int x, int y)
        {
            return _objects.SingleOrDefault(obj => obj.XPosition == x && obj.YPosition == y);
        }

        public bool ObjectsRemaining<T>() where T : IPositionableObject
        {
            return _objects.OfType<T>().Any();
        }

        #endregion
    }
}
