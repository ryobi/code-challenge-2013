﻿using System;

namespace ToyRobot.Model
{
    public class Robot : PositionableObject, IRobot
    {

        #region Fields & Constants

        private const string ReportFormat = "({0}, {1}) {2}";
        private const string InvalidReport = "INVALID ROBOT PLACEMENT";

        public Robot(IPlayingSurface surface) : base(surface)
        {
            IsPlaced = false;
        }

        #endregion

        #region Properties

        /// <summary>
        /// The current direction the robot is facing
        /// </summary>
        public Direction FacingDirection { get; private set; }

        #endregion

        #region Methods

        /// <summary>
        /// Reset the position and direction of the robot
        /// </summary>
        /// <param name="x">The X position to place the robot</param>
        /// <param name="y">The Y position to place the robot</param>
        /// <param name="directionFacing">The direction the robot should be facing when placed</param>
        public bool Place(int x, int y, Direction directionFacing)
        {
            if (CurrentPlayingSurface().IsObjectAtPosition(x, y))
                return false;

            var didInitialPlacement = Place(x, y);

            if (!didInitialPlacement)
                return false;

            FacingDirection = directionFacing;
            IsPlaced = true;

            return true;
        }

        /// <summary>
        /// Move the robot forward the number of provided steps in the direction it is currently facing
        /// </summary>
        /// <param name="steps">The number of steps (in a plane) to move the robot</param>
        public void Move(int steps)
        {
            if (!IsPlaced)
                return;

            int newYPos = YPosition;
            int newXPos = XPosition;

            // Switch statements are simple, m'kay?
            switch (FacingDirection)
            {
                case Direction.North:
                    newYPos += steps;
                    break;
                case Direction.West:
                    newXPos -= steps;
                    break;
                case Direction.South:
                    newYPos -= steps;
                    break;
                case Direction.East:
                    newXPos += steps;
                    break;
                default:
                    throw new InvalidOperationException("Facing Direction is not a valid enumeration!");
            }

            if (CurrentPlayingSurface().IsObjectAtPosition(newXPos, newYPos))
                return;

            YPosition = newYPos;
            XPosition = newXPos;
        }

        /// <summary>
        /// Move the toy robot by 90 degrees to the left of it's current position
        /// </summary>
        public void Left()
        {
            if (!IsPlaced)
                return;

            // This (like it's Right counterpart) could be done a million ways better.. but, KISS, right? :-)
            switch (FacingDirection)
            {
                case Direction.North:
                    FacingDirection = Direction.West;
                    break;
                case Direction.West:
                    FacingDirection = Direction.South;
                    break;
                case Direction.South:
                    FacingDirection = Direction.East;
                    break;
                case Direction.East:
                    FacingDirection = Direction.North;
                    break;
                default:
                    throw new InvalidOperationException("Facing Direction is not a valid enumeration!");
            }
        }

        /// <summary>
        /// Move the toy robot by 90 degrees to the right of it's current position
        /// </summary>
        public void Right()
        {
            if (!IsPlaced)
                return;

            // This (like it's Left counterpart) could be done a million ways better.. but, KISS, right? :-)
            switch (FacingDirection)
            {
                case Direction.North:
                    FacingDirection = Direction.East;
                    break;
                case Direction.East:
                    FacingDirection = Direction.South;
                    break;
                case Direction.South:
                    FacingDirection = Direction.West;
                    break;
                case Direction.West:
                    FacingDirection = Direction.North;
                    break;
                default:
                    throw new InvalidOperationException("Facing Direction is not a valid enumeration!");
            }
        }

        public bool Clean()
        {
            var inFront = CoOrdinatesInFront();
            if (CurrentPlayingSurface().IsObjectAtPosition(inFront.Item1, inFront.Item2))
                CurrentPlayingSurface()
                    .RemoveObjectFromPlayingSurface(CurrentPlayingSurface()
                        .GetObjectAtPosition(inFront.Item1, inFront.Item2));

            return !CurrentPlayingSurface().ObjectsRemaining<IObsticle>();
        }

        private Tuple<int, int> CoOrdinatesInFront()
        {
            switch (FacingDirection)
            {
                case Direction.North:
                    return Tuple.Create(XPosition, YPosition + 1);
                case Direction.West:
                    return Tuple.Create(XPosition - 1, YPosition);
                case Direction.South:
                    return Tuple.Create(XPosition, YPosition - 1);
                case Direction.East:
                    return Tuple.Create(XPosition + 1, YPosition);
                default:
                    throw new InvalidOperationException("Facing Direction is not a valid enumeration!");
            }
        }

        /// <summary>
        /// Gives the current co-ordinates of the robot, along with the direction it is facing.
        /// </summary>
        public string Report()
        {
            if (!IsPlaced)
                return InvalidReport;

            return string.Format(ReportFormat, XPosition, YPosition, FacingDirection.ToString().ToUpperInvariant());
        }

        #endregion

    }
}
