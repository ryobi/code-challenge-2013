﻿using Moq;
using NUnit.Framework;
using ToyRobot.Model;

namespace ToyRobotTests.Model
{
    [TestFixture]
    class TableTests
    {

        #region Fields

        private IPlayingSurface _surface;

        private const int Width = 15;
        private const int Length = 10;

        #endregion

        #region Infrastructure

        [SetUp]
        public void PreTestSetup()
        {
            _surface = new Table(Length, Width);
        }

        [TearDown]
        public void PostTestTearDown()
        {
            _surface = null;
        }

        #endregion

        #region Surface Dimensions

        [Test]
        public void GetWidth_ReturnsWidth()
        {
            // Arrange
            
            // Act
            int width = _surface.SurfaceWidth();

            // Assert
            Assert.AreEqual(Width, width);
        }

        [Test]
        public void GetLength_ReturnsLength()
        {
            // Arrange

            // Act
            int length = _surface.SurfaceLength();

            // Assert
            Assert.AreEqual(Length, length);
        }

        #endregion

        #region Objects On Surface

        [Test]
        public void RetrieveObjectAfterAdding_CorrectObjectRetrieved()
        {
            // Arrange
            const int xPos = 4;
            const int yPos = 2;
            var mockObject = new Mock<IPositionableObject>();
            mockObject.Setup(x => x.XPosition).Returns(xPos);
            mockObject.Setup(x => x.YPosition).Returns(yPos);
            
            // Act
            _surface.AddObjectToPlayingSurface(mockObject.Object);
            var obj = _surface.GetObjectAtPosition(xPos, yPos);

            // Assert
            Assert.AreSame(mockObject.Object, obj);
        }

        [Test]
        public void AddTwoObjectsAtSamePosition_SecondAddFails()
        {
            // Arrange
            const int xPos = 4;
            const int yPos = 2;
            var firstMockObject = new Mock<IPositionableObject>();
            firstMockObject.Setup(x => x.XPosition).Returns(xPos);
            firstMockObject.Setup(x => x.YPosition).Returns(yPos);

            var secondMockObject = new Mock<IPositionableObject>();
            secondMockObject.Setup(x => x.XPosition).Returns(xPos);
            secondMockObject.Setup(x => x.YPosition).Returns(yPos);

            // Act
            _surface.AddObjectToPlayingSurface(firstMockObject.Object);
            var result = _surface.AddObjectToPlayingSurface(secondMockObject.Object);

            // Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void TestForObjectAtExistingPosition_ReturnsTrue()
        {
            // Arrange
            const int xPos = 4;
            const int yPos = 2;
            var mockObject = new Mock<IPositionableObject>();
            mockObject.Setup(x => x.XPosition).Returns(xPos);
            mockObject.Setup(x => x.YPosition).Returns(yPos);

            // Act
            _surface.AddObjectToPlayingSurface(mockObject.Object);

            // Assert
            Assert.IsTrue(_surface.IsObjectAtPosition(xPos, yPos));
        }

        [Test]
        public void RemoveExistingObject_ObjectRemoved()
        {
            // Arrange
            const int xPos = 4;
            const int yPos = 2;
            var mockObject = new Mock<IPositionableObject>();
            mockObject.Setup(x => x.XPosition).Returns(xPos);
            mockObject.Setup(x => x.YPosition).Returns(yPos);
            _surface.AddObjectToPlayingSurface(mockObject.Object);

            // Act
            _surface.RemoveObjectFromPlayingSurface(mockObject.Object);

            // Assert
            Assert.IsFalse(_surface.IsObjectAtPosition(xPos, yPos));
        }

        #endregion

    }
}
