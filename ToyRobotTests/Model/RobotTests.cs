﻿using Moq;
using NUnit.Framework;
using ToyRobot.Model;

namespace ToyRobotTests.Model
{
    [TestFixture]
    public class RobotTests
    {

        #region Fields

        private IRobot _robot;
        private Mock<IPlayingSurface> _mockSurface;

        private const int DefaultX = 10;
        private const int DefaultY = 10;
        private const Direction DefaultDirection = Direction.North;
        private const string ReportFormat = "({0}, {1}) {2}";

        #endregion

        #region Infrastructure

        [SetUp]
        public void PreTestSetup()
        {
            _mockSurface = new Mock<IPlayingSurface>();
            _mockSurface.Setup(x => x.SurfaceLength()).Returns(10);
            _mockSurface.Setup(x => x.SurfaceWidth()).Returns(10);

            _robot = new Robot(_mockSurface.Object);
        }

        [TearDown]
        public void PostTestTearDown()
        {
            _robot = null;
            _mockSurface = null;
        }

        #endregion

        #region Object Instantiation

        [Test]
        public void CreateRobot_IsAtOriginX()
        {
            // Arrange
            const int expectedX = 0;

            // Act
            // Do nothing

            // Assert
            Assert.AreEqual(expectedX, _robot.XPosition);
        }

        [Test]
        public void CreateRobot_IsAtOriginY()
        {
            // Arrange
            const int expectedY = 0;

            // Act
            // Do nothing

            // Assert
            Assert.AreEqual(expectedY, _robot.YPosition);
        }

        [Test]
        public void GetPlayingSurface_SurfaceReturned()
        {
            // Arrange
            var mockSurface = new Mock<IPlayingSurface>();
            mockSurface.Setup(x => x.SurfaceLength()).Returns(10);
            mockSurface.Setup(x => x.SurfaceWidth()).Returns(10);

            _robot = new Robot(mockSurface.Object);

            // Act
            var surface = _robot.CurrentPlayingSurface();

            // Assert
            Assert.AreEqual(mockSurface.Object, surface);
        }

        #endregion

        #region Placement

        [Test]
        public void PlaceAtXPosition_IsAtXPosition()
        {
            // Arrange
            const int expectedX = 5;

            // Act
            _robot.Place(expectedX, DefaultY, DefaultDirection);

            // Assert
            Assert.AreEqual(expectedX, _robot.XPosition);
        }

        [Test]
        public void PlaceAtXPosition_YPositionUnchanged()
        {
            // Arrange
            const int newX = 5;

            // Act
            _robot.Place(newX, DefaultY, DefaultDirection);

            // Assert
            Assert.AreEqual(DefaultY, _robot.YPosition);
        }

        [Test]
        public void PlaceAtYPosition_IsAtYPosition()
        {
            // Arrange
            const int expectedY = 5;

            // Act
            _robot.Place(DefaultX, expectedY, DefaultDirection);

            // Assert
            Assert.AreEqual(expectedY, _robot.YPosition);
        }

        [Test]
        public void PlaceAtYPosition_XPositionUnchanged()
        {
            // Arrange
            const int newY = 5;

            // Act
            _robot.Place(DefaultX, newY, DefaultDirection);

            // Assert
            Assert.AreEqual(DefaultX, _robot.XPosition);
        }

        [Test]
        public void PlaceAtNewPosition_DirectionSet()
        {
            // Arrange
            const Direction expectedDirection = Direction.West;

            // Act
            _robot.Place(DefaultX, DefaultY, expectedDirection);

            // Assert
            Assert.AreEqual(expectedDirection, _robot.FacingDirection);
        }

        [Test]
        public void PlaceAtInvalidYPosition_YPositionUnchanged()
        {
            // Arrange
            const int newY = 15;

            // Act
            _robot.Place(DefaultX, DefaultY, DefaultDirection);
            _robot.Place(DefaultX, newY, DefaultDirection);

            // Assert
            Assert.AreEqual(DefaultY, _robot.YPosition);  
        }

        [Test]
        public void PlaceAtInvalidXPosition_XPositionUnchanged()
        {
            // Arrange
            const int newX = -1;

            // Act
            _robot.Place(DefaultX, DefaultY, DefaultDirection);
            _robot.Place(newX, DefaultY, DefaultDirection);

            // Assert
            Assert.AreEqual(DefaultX, _robot.XPosition);
        }

        [Test]
        public void PlaceAtInvalidPositionAfterCorrectPosition_PositionUnchangedFromCorrectPosition()
        {
            // Arrange
            const int correctX = 5;
            const int correctY = 6;
            const Direction correctDirection = Direction.East;
            const int invalidX = 11;
            const int invalidY = -5;
            const Direction invalidDirection = Direction.West;

            // Act
            _robot.Place(correctX, correctY, correctDirection);
            _robot.Place(invalidX, invalidY, invalidDirection);

            // Assert
            Assert.AreEqual(correctX, _robot.XPosition);
            Assert.AreEqual(correctY, _robot.YPosition);
            Assert.AreEqual(correctDirection, _robot.FacingDirection);
        }

        [Test]
        public void PlaceAtInvalidPositionForCurrentSurface_IsNotPlaced()
        {
            // Arrange
            const int newPos = 3;
            const int boundaryPos = 2;

            _mockSurface.Setup(x => x.SurfaceWidth()).Returns(boundaryPos);
            _mockSurface.Setup(x => x.SurfaceLength()).Returns(boundaryPos);

            // Act
            _robot.Place(boundaryPos, boundaryPos, DefaultDirection);
            _robot.Place(newPos, newPos, DefaultDirection);

            // Assert
            Assert.AreEqual(boundaryPos, _robot.YPosition);
            Assert.AreEqual(boundaryPos, _robot.XPosition);
        }

        [Test]
        public void PlaceOnTopOfObsticle_IsNotPlaced()
        {
            // Arrange
            const int obsticlePlacement = 1;
            var mockObsticle = new Mock<IObsticle>();

            mockObsticle.Setup(x => x.CurrentPlayingSurface()).Returns(_mockSurface.Object);

            _mockSurface.Setup(x => x.IsObjectAtPosition(obsticlePlacement, obsticlePlacement)).Returns(true);
            _mockSurface.Setup(x => x.GetObjectAtPosition(obsticlePlacement, obsticlePlacement)).Returns(mockObsticle.Object);
            
            // Act
            var result = _robot.Place(obsticlePlacement, obsticlePlacement, DefaultDirection);

            // Assert
            Assert.IsFalse(result);
        }

        #endregion

        #region Movement

        [Test]
        public void MoveToNewPositionToWest_XPositionChanged()
        {
            // Arrange
            const Direction expectedDirection = Direction.West;
            const int placesToMove = 8;
            const int expectedX = 2;

            // Act
            _robot.Place(10,10, expectedDirection);
            _robot.Move(placesToMove);

            // Assert
            Assert.AreEqual(expectedX, _robot.XPosition);
        }

        [Test]
        public void MoveToNewPositionToWest_YPositionChanged()
        {
            // Arrange
            const Direction expectedDirection = Direction.West;
            const int placesToMove = 8;
            const int expectedY = 0;

            // Act
            _robot.Place(0, 0, expectedDirection);
            _robot.Move(placesToMove);

            // Assert
            Assert.AreEqual(expectedY, _robot.YPosition);
        }

        [Test]
        public void MoveToNewPositionToEast_XPositionChanged()
        {
            // Arrange
            const Direction expectedDirection = Direction.East;
            const int placesToMove = 8;
            const int expectedX = 8;

            // Act
            _robot.Place(0, 0, expectedDirection);
            _robot.Move(placesToMove);

            // Assert
            Assert.AreEqual(expectedX, _robot.XPosition);
        }

        [Test]
        public void MoveToNewPositionToEast_YPositionChanged()
        {
            // Arrange
            const Direction expectedDirection = Direction.East;
            const int placesToMove = 8;
            const int expectedY = 0;

            // Act
            _robot.Place(0, 0, expectedDirection);
            _robot.Move(placesToMove);

            // Assert
            Assert.AreEqual(expectedY, _robot.YPosition);
        }

        [Test]
        public void MoveToNewPositionToNorth_XPositionChanged()
        {
            // Arrange
            const Direction expectedDirection = Direction.North;
            const int placesToMove = 8;
            const int expectedX = 0;

            // Act
            _robot.Place(0, 0, expectedDirection);
            _robot.Move(placesToMove);

            // Assert
            Assert.AreEqual(expectedX, _robot.XPosition);
        }

        [Test]
        public void MoveToNewPositionToNorth_YPositionChanged()
        {
            // Arrange
            const Direction expectedDirection = Direction.North;
            const int placesToMove = 8;
            const int expectedY = 8;

            // Act
            _robot.Place(0, 0, expectedDirection);
            _robot.Move(placesToMove);

            // Assert
            Assert.AreEqual(expectedY, _robot.YPosition);
        }

        [Test]
        public void MoveToNewPositionToSouth_XPositionChanged()
        {
            // Arrange
            const Direction expectedDirection = Direction.South;
            const int placesToMove = 8;
            const int expectedX = 0;

            // Act
            _robot.Place(0, 0, expectedDirection);
            _robot.Move(placesToMove);

            // Assert
            Assert.AreEqual(expectedX, _robot.XPosition);
        }

        [Test]
        public void MoveToNewPositionToSouth_YPositionChanged()
        {
            // Arrange
            const Direction expectedDirection = Direction.South;
            const int placesToMove = 8;
            const int expectedY = 2;

            // Act
            _robot.Place(10, 10, expectedDirection);
            _robot.Move(placesToMove);

            // Assert
            Assert.AreEqual(expectedY, _robot.YPosition);
        }


        [Test]
        public void MoveLeftFromNorth_DirectionIsWest()
        {
            // Arrange
            const Direction startingDirection = Direction.North;
            const Direction expectedDirection = Direction.West;

            // Act
            _robot.Place(0, 0, startingDirection);
            _robot.Left();

            // Assert
            Assert.AreEqual(expectedDirection, _robot.FacingDirection);
        }

        [Test]
        public void MoveLeftFromEast_DirectionIsNorth()
        {
            // Arrange
            const Direction startingDirection = Direction.East;
            const Direction expectedDirection = Direction.North;

            // Act
            _robot.Place(0, 0, startingDirection);
            _robot.Left();

            // Assert
            Assert.AreEqual(expectedDirection, _robot.FacingDirection);
        }

        [Test]
        public void MoveRightFromNorth_DirectionIsEast()
        {
            // Arrange
            const Direction startingDirection = Direction.North;
            const Direction expectedDirection = Direction.East;

            // Act
            _robot.Place(0, 0, startingDirection);
            _robot.Right();

            // Assert
            Assert.AreEqual(expectedDirection, _robot.FacingDirection);
        }

        [Test]
        public void MoveRightFromSouth_DirectionIsWest()
        {
            // Arrange
            const Direction startingDirection = Direction.South;
            const Direction expectedDirection = Direction.West;

            // Act
            _robot.Place(0, 0, startingDirection);
            _robot.Right();

            // Assert
            Assert.AreEqual(expectedDirection, _robot.FacingDirection);
        }

        [Test]
        public void MoveBeforePlaceCalled_NoMovement()
        {
            // Arrange
            const int steps = 1;
            const int startingPos = 0;

            // Act
            _robot.Move(steps);

            // Assert
            Assert.AreEqual(startingPos, _robot.XPosition);
            Assert.AreEqual(startingPos, _robot.YPosition);
        }

        [Test]
        public void MoveIntoInvalidArea_NoMovement()
        {
            // Arrange
            const int steps = -1;
            const int boundaryPosition = 0;

            _robot.Place(boundaryPosition, boundaryPosition, DefaultDirection);

            // Act
            _robot.Move(steps);

            // Assert
            Assert.AreEqual(boundaryPosition, _robot.XPosition);
            Assert.AreEqual(boundaryPosition, _robot.YPosition);
        }

        [Test]
        public void MoveOnTopOfObsticle_IsNotMoved()
        {
            // Arrange
            const int obsticlePlacement = 1;
            var mockObsticle = new Mock<IObsticle>();

            mockObsticle.Setup(x => x.CurrentPlayingSurface()).Returns(_mockSurface.Object);

            _mockSurface.Setup(x => x.IsObjectAtPosition(It.IsAny<int>(), obsticlePlacement)).Returns(true);
            _mockSurface.Setup(x => x.GetObjectAtPosition(It.IsAny<int>(), obsticlePlacement)).Returns(mockObsticle.Object);
            _robot.Place(0, 0, Direction.North);

            // Act
            _robot.Move(1);

            // Assert
            Assert.AreEqual(0, _robot.XPosition);
            Assert.AreEqual(0, _robot.YPosition);
        }

        #endregion

        #region Reporting
        
        [Test]
        public void ReportStartingDirection_CorrectReport()
        {
            // Arrange
            string expectedReport = string.Format(ReportFormat,0,0,"NORTH");

            // Act
            _robot.Place(0,0,Direction.North);
            var report = _robot.Report();

            // Assert
            Assert.AreEqual(expectedReport, report);
        }

        [Test]
        public void ReportNewPlacement_CorrectReport()
        {
            // Arrange
            string expectedReport = string.Format(ReportFormat, 6, 9, "SOUTH");

            // Act
            _robot.Place(6, 9, Direction.South);
            var report = _robot.Report();

            // Assert
            Assert.AreEqual(expectedReport, report);
        }

        [Test]
        public void ReportNewPositionAfterMovement_CorrectReport()
        {
            // Arrange
            string expectedReport = string.Format(ReportFormat, 8, 9, "EAST");

            // Act
            _robot.Place(6, 9, Direction.South);
            _robot.Left();
            _robot.Move(2);
            var report = _robot.Report();

            // Assert
            Assert.AreEqual(expectedReport, report);
        }

        [Test]
        public void ReportNewPositionAfterInvalidMovement_CorrectReport()
        {
            // Arrange
            string expectedReport = string.Format(ReportFormat, 6, 9, "EAST");

            // Act
            _robot.Place(6, 9, Direction.South);
            _robot.Left();
            _robot.Move(10);
            var report = _robot.Report();

            // Assert
            Assert.AreEqual(expectedReport, report);
        }

        [Test]
        public void ReportNewPositionAfterMovementBeforeBeingPlaced_ReportInvalid()
        {
            // Arrange
            const string expectedReport = "INVALID ROBOT PLACEMENT";

            // Act
            _robot.Left();
            _robot.Move(10);
            var report = _robot.Report();

            // Assert
            Assert.AreEqual(expectedReport, report);
        }

        #endregion

        #region Cleaning

        [Test]
        public void CleanObsticleInFront_CorrectCleanCalled()
        {
            // Arrange
            const int dirtX = 5;
            const int dirtY = 5;
            
            var mockDirt = new Mock<IObsticle>();
            _mockSurface.Setup(x => x.IsObjectAtPosition(dirtX, dirtY)).Returns(true);
            _mockSurface.Setup(x => x.GetObjectAtPosition(dirtX, dirtY)).Returns(mockDirt.Object);
            _mockSurface.Setup(x => x.RemoveObjectFromPlayingSurface(It.IsAny<IPositionableObject>())).Returns(true);


            // Act
            _robot.Place(dirtX - 1, dirtY, Direction.East);
            _robot.Clean();

            // Assert
            _mockSurface.Verify(x => x.RemoveObjectFromPlayingSurface(mockDirt.Object), Times.Exactly(1));
        }

        [Test]
        public void CleanWithoutObsticleInFront_CleanUnsuccessful()
        {
            // Arrange
            const int dirtX = 5;
            const int dirtY = 5;

            var mockDirt = new Mock<IObsticle>();
            _mockSurface.Setup(x => x.IsObjectAtPosition(dirtX, dirtY)).Returns(true);
            _mockSurface.Setup(x => x.GetObjectAtPosition(dirtX, dirtY)).Returns(mockDirt.Object);
            _mockSurface.Setup(x => x.RemoveObjectFromPlayingSurface(mockDirt.Object)).Returns(true);

            // Act
            _robot.Place(dirtX+1, dirtY+1, Direction.East);
            _robot.Clean();

            // Assert
            _mockSurface.Verify(x => x.RemoveObjectFromPlayingSurface(It.IsAny<IPositionableObject>()), Times.Never);
        }

        [Test]
        public void CleanAllDirt_GameOverRecieved()
        {
            // Arrange
            const int dirtX = 5;
            const int dirtY = 5;

            var mockDirt = new Mock<IObsticle>();
            _mockSurface.Setup(x => x.IsObjectAtPosition(dirtX, dirtY)).Returns(true);
            _mockSurface.Setup(x => x.GetObjectAtPosition(dirtX, dirtY)).Returns(mockDirt.Object);
            _mockSurface.Setup(x => x.RemoveObjectFromPlayingSurface(mockDirt.Object)).Returns(true);
            _mockSurface.Setup(x => x.ObjectsRemaining<IObsticle>()).Returns(true);

            // Act
            _robot.Place(dirtX - 1, dirtY, Direction.East);
            _mockSurface.Setup(x => x.ObjectsRemaining<IObsticle>()).Returns(false);
            var result = _robot.Clean();

            // Assert
            Assert.IsTrue(result);
        }

        [Test]
        public void CleanSomeDirt_GameOverNotRecieved()
        {
            // Arrange
            const int dirtX = 5;
            const int dirtY = 5;

            var mockDirt = new Mock<IObsticle>();
            _mockSurface.Setup(x => x.IsObjectAtPosition(dirtX, dirtY)).Returns(true);
            _mockSurface.Setup(x => x.GetObjectAtPosition(dirtX, dirtY)).Returns(mockDirt.Object);
            _mockSurface.Setup(x => x.RemoveObjectFromPlayingSurface(mockDirt.Object)).Returns(true);

            var mockDirtTwo = new Mock<IObsticle>();
            _mockSurface.Setup(x => x.IsObjectAtPosition(dirtX + 2, dirtY + 2)).Returns(true);
            _mockSurface.Setup(x => x.GetObjectAtPosition(dirtX + 2, dirtY + 2)).Returns(mockDirt.Object);
            _mockSurface.Setup(x => x.RemoveObjectFromPlayingSurface(mockDirtTwo.Object)).Returns(true);


            _mockSurface.Setup(x => x.ObjectsRemaining<IObsticle>()).Returns(true);

            // Act
            _robot.Place(dirtX - 1, dirtY, Direction.East);
            _mockSurface.Setup(x => x.ObjectsRemaining<IObsticle>()).Returns(true);
            var result = _robot.Clean();
            

            // Assert
            Assert.IsFalse(result);
        }

        #endregion
    }
}