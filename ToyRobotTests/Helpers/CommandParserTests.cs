﻿using System;
using Moq;
using NUnit.Framework;
using ToyRobot.Model;
using ToyRobot.Helpers;

namespace ToyRobotTests.Helpers
{
    [TestFixture]
    public class CommandParserTests
    {
        #region Fields

        private Mock<IRobot> _mockRobot;
        private ICommandParser _parser;

        private const string PlacementFormat = "PLACE {0},{1},{2}";

        #endregion

        #region Infrastructure

        [SetUp]
        public void PreTestSetup()
        {
            _parser = new CommandParser();
            _mockRobot = new Mock<IRobot>();

            _mockRobot.SetupAllProperties();
        }

        #endregion

        #region Parsing

        #region AcceptingCommands

        [Test]
        public void ParseMoveCommand_CommandAccepted()
        {
            // Arrange
            const string command = "MOVE";

            // Act
            _parser.ParseCommand(_mockRobot.Object, command);

            // Assert
            Assert.Pass();
        }

        [Test]
        public void ParseLeftCommand_CommandAccepted()
        {
            // Arrange
            const string command = "LEFT";

            // Act
            _parser.ParseCommand(_mockRobot.Object, command);

            // Assert
            Assert.Pass();
        }

        [Test]
        public void ParseRightCommand_CommandAccepted()
        {
            // Arrange
            const string command = "RIGHT";

            // Act
            _parser.ParseCommand(_mockRobot.Object, command);

            // Assert
            Assert.Pass();
        }

        [Test]
        public void ParsePlaceCommand_CommandAccepted()
        {
            // Arrange
            string command = string.Format(PlacementFormat,30,30,"NORTH");

            // Act
            _parser.ParseCommand(_mockRobot.Object, command);

            // Assert
            Assert.Pass();
        }

        [Test]
        public void ParseReportCommand_CommandAccepted()
        {
            // Arrange
            const string command = "REPORT";

            // Act
             _parser.ParseCommand(_mockRobot.Object, command);

            // Assert
            Assert.Pass();
        }

        [Test, ExpectedException(typeof(ArgumentException))]
        public void ParseInvalidCommand_CommandRejected()
        {
            // Arrange
            const string command = "INV COM 123";

            // Act
            _parser.ParseCommand(_mockRobot.Object, command);

            // Assert
            Assert.Fail();
        }

        #endregion

        #region CleanCommand

        [Test]
        public void ParseCleanCommand_CleanFunctionCalled()
        {
            // Arrange
            const string command = "CLEAN";

            // Act
            _parser.ParseCommand(_mockRobot.Object, command);

            // Assert
            _mockRobot.Verify(x => x.Clean(), Times.Exactly(1));
        }


        #endregion

        #endregion
    }
}
